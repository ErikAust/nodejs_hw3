/* eslint-disable camelcase */
/* eslint object-curly-spacing: ["error", "always"] */

const express = require('express');
const router = new express.Router();

const { usersRole } = require('../config');

const {
  addLoadForUser,
  getLoads,
  getUsersActiveLoad,
  changeLoadState,
  getUsersloadById,
  updateUsersLoadById,
  deleteUsersLoadById,
  postUsersLoadById,
  getUsersloadShippingById,
} = require('../services/loadService');

const { asyncWrapper } = require('../utils/apiUtils');

router.get('/', asyncWrapper(async (req, res) => {
  const { created_by, role, email } = req.user;
  const { offset, limit, status } = req.query;

  const loads = await getLoads(created_by, offset, limit, status, role);

  if (req.headers.cookie || req.headers.pragma) {
    const newLoads = [];
    loads.forEach((elem) => {
      if (elem.status === 'NEW') {
        newLoads.push(elem);
      }
    });

    res.render('index', {
      title: 'New Loads',
      isNew: true,
      isIndex: false,
      LOGIN: true,
      newLoads,
      email,
    });
  } else {
    res.status(200).json({ loads });
  };
}));

router.post('/', asyncWrapper(async (req, res) => {
  const { created_by, role, email } = req.user;

  if (role !== usersRole.SHIPPER) {
    throw new Error('Your role is not authorized to perform these actions');
  };

  const data = req.body;

  await addLoadForUser(created_by, data);

  if (req.headers.cookie || req.headers.pragma) {
    res.render('index', {
      title: 'Load created',
      isCreated: true,
      LOGIN: true,
      isIndex: false,
      email,
    });
  } else {
    res.status(200).json({ message: 'Load created successfully' });
  }
}));

router.get('/active', asyncWrapper(async (req, res) => {
  const { created_by, role } = req.user;

  if (role !== usersRole.DRIVER) {
    throw new Error('Your role is not authorized to perform these actions');
  };

  const load = await getUsersActiveLoad(created_by);

  res.status(200).json({ load });
}));

router.patch('/active/state', asyncWrapper(async (req, res) => {
  const { created_by, role } = req.user;

  if (role !== usersRole.DRIVER) {
    throw new Error('Your role is not authorized to perform these actions');
  };

  const state = await changeLoadState(created_by);

  res.status(200).json({ message: `Load state changed to '${state}'` });
}));

router.get('/:id', asyncWrapper(async (req, res) => {
  const { created_by } = req.user;
  const id = req.params.id;

  const load = await getUsersloadById(id, created_by);

  res.status(200).json({ load });
}));

router.put('/:id', asyncWrapper(async (req, res) => {
  const { created_by, role } = req.user;

  if (role !== usersRole.SHIPPER) {
    throw new Error('Your role is not authorized to perform these actions');
  };

  const id = req.params.id;
  const data = req.body;

  await updateUsersLoadById(id, created_by, data);

  res.status(200).json({ message: 'Load details changed successfully' });
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
  const { created_by, role } = req.user;

  if (role !== usersRole.SHIPPER) {
    throw new Error('Your role is not authorized to perform these actions');
  };

  const id = req.params.id;

  await deleteUsersLoadById(id, created_by);

  res.status(200).json({ message: 'Load deleted successfully' });
}));

router.post('/:id/post', asyncWrapper(async (req, res) => {
  const { role, email } = req.user;

  if (role !== usersRole.SHIPPER) {
    throw new Error('Your role is not authorized to perform these actions');
  };

  const id = req.params.id;

  const driverFound = await postUsersLoadById(id);

  if (req.headers.cookie || req.headers.pragma) {
    res.render('index', {
      title: 'Post load',
      isPosted: true,
      LOGIN: true,
      isIndex: false,
      driverFound,
      email,
    });
  } else {
    res.status(200).json({
      message: 'Load posted successfully',
      driver_found: driverFound,
    });
  }
}));

router.get('/:id/shipping_info', asyncWrapper(async (req, res) => {
  const { created_by, role } = req.user;

  if (role !== usersRole.SHIPPER) {
    throw new Error('Your role is not authorized to perform these actions');
  };

  const id = req.params.id;

  const load = await getUsersloadShippingById(id, created_by);

  res.status(200).json({ load });
}));

module.exports = {
  loadRouter: router,
};
