/* eslint-disable camelcase */
/* eslint object-curly-spacing: ["error", "always"] */

const express = require('express');
const router = new express.Router();

const { asyncWrapper } = require('../utils/apiUtils');
const { Load } = require('../models/loadModel');

router.get('/', (req, res) => {
  res.render('index', {
    title: 'Load Service',
    isIndex: true,
    LOGIN: false,
    isLog: false,
  });
});

router.get('/reg', (req, res) => {
  res.render('index', {
    title: 'Registration',
    isReg: true,
    isIndex: false,
    LOGIN: false,
    isLog: false,
  });
});

router.get('/log', (req, res) => {
  res.render('index', {
    title: 'LOGIN',
    isLoged: false,
    isLog: true,
    isIndex: false,
  });
});

router.get('/api/create', (req, res) => {
  res.render('index', {
    title: 'Create new load',
    isCreate: true,
    LOGIN: true,
    isIndex: false,
    isLog: false,
  });
});

router.get('/api/editload/:id', (req, res) => {
  const id = req.params.id;
  res.render('index', {
    title: 'Editing load',
    isEdit: true,
    LOGIN: true,
    isIndex: false,
    isLog: false,
    ID: id,
  });
});

router.post('/api/edited/:id', asyncWrapper(async (req, res) => {
  const id = req.params.id;
  const data = req.body;

  for (key in data) {
    if (data[key] === '') {
      delete data[key];
    }
  };

  const load = await Load.findOne({ _id: id });

  if (load.status !== 'NEW') {
    throw new Error(`You can only update info about loads with status 'NEW'`);
  } else {
    await Load.findOneAndUpdate({ _id: id }, { $set: data });

    res.render('index', {
      editFlag: true,
      LOGIN: true,
      isIndex: false,
      isLog: false,
    });
  }
}));

router.get('/api/loads/shipping_info', asyncWrapper(async (req, res) => {
  const load = await Load.findOne({ status: 'ASSIGNED' });

  res.render('index', {
    title: 'Assigned load',
    isAssigned: true,
    LOGIN: true,
    isIndex: false,
    isLog: false,
    load,
  });
}));

router.get('/api/history', asyncWrapper(async (req, res) => {
  const loads = await Load.find({ status: 'SHIPPED' });

  if (!loads) {
    throw new Error('No exist loads shipping');
  } else {
    res.render('index', {
      title: 'History',
      isHistory: true,
      isIndex: false,
      LOGIN: true,
      isLog: false,
      loads,
    });
  }
}));

router.post('/api/:id/delete', asyncWrapper(async (req, res) => {
  const id = req.params.id;

  await Load.findOneAndRemove({ _id: id });

  res.render('index', {
    title: 'Load deleted',
    isDelete: true,
    LOGIN: true,
    isIndex: false,
    isLog: false,
  });
}));

router.get('/api/forgot', asyncWrapper(async (req, res) => {
  res.render('index', {
    title: 'Forgot password',
    isForgot: true,
    isIndex: false,
    isLog: false,
  });
}));
module.exports = { mainRouter: router };
