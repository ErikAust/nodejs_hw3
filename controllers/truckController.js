/* eslint-disable camelcase */
/* eslint object-curly-spacing: ["error", "always"] */

const express = require('express');
const router = new express.Router();

const {
  getUsersTrucks,
  addTruckForUser,
  getUsersTruckById,
  updateUsersTruckById,
  deleteUsersTruckById,
  assignTruckToUserById,
} = require('../services/truckService');

const { asyncWrapper } = require('../utils/apiUtils');

const { SPRINTER,
  SMALLSTRAIGHT,
  LARGESTRAIGHT } = require('../config');

router.get('/', asyncWrapper(async (req, res) => {
  const { created_by } = req.user;

  const trucks = await getUsersTrucks(created_by);

  res.status(200).json({ trucks });
}));

router.post('/', asyncWrapper(async (req, res) => {
  const { created_by } = req.user;

  let type = req.body;

  switch (type.type) {
    case 'SPRINTER':
      type = SPRINTER;
      break;
    case 'SMALL STRAIGHT':
      type = SMALLSTRAIGHT;
      break;
    case 'LARGE STRAIGHT':
      type = LARGESTRAIGHT;
      break;
    default:
      break;
  };

  await addTruckForUser(created_by, type);

  res.status(200).json({ message: 'Truck created successfully' });
}));

router.get('/:id', asyncWrapper(async (req, res) => {
  const { created_by } = req.user;

  const id = req.params.id;

  const truck = await getUsersTruckById(id, created_by);

  res.status(200).json({ truck });
}));

router.put('/:id', asyncWrapper(async (req, res) => {
  const { created_by } = req.user;

  const id = req.params.id;
  const data = req.body;

  await updateUsersTruckById(id, created_by, data);

  res.status(200).json({ message: 'Truck details changed successfully' });
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
  const { created_by } = req.user;

  const id = req.params.id;

  await deleteUsersTruckById(id, created_by);

  res.status(200).json({ message: 'Truck deleted successfully' });
}));

router.post('/:id/assign', asyncWrapper(async (req, res) => {
  const { created_by } = req.user;

  const id = req.params.id;

  await assignTruckToUserById(id, created_by);

  res.status(200).json({ message: 'Truck assigned successfully' });
}));

module.exports = {
  truckRouter: router,
};
