/* eslint object-curly-spacing: ["error", "always"] */

const express = require('express');
const router = new express.Router();

const {
  register,
  login,
  changePassword,
} = require('../services/authService');

const { asyncWrapper } = require('../utils/apiUtils');

router.post('/register', asyncWrapper(async (req, res) => {
  const {
    email,
    password,
    role,
  } = req.body;

  await register({ email, password, role });

  if (req.headers.cookie || req.headers.pragma) {
    res.render('index', {
      isReged: true,
      isIndex: false,
    });
  } else {
    res.status(200).json({ message: 'Profile created successfully' });
  }
}));

router.post('/login', asyncWrapper(async (req, res) => {
  const {
    email,
    password,
  } = req.body;

  const token = await login({ email, password });

  if (req.headers.cookie || req.headers.pragma) {
    res.cookie('jwt_token', `${token}`).render('index', {
      isLoged: true,
      LOGIN: true,
      isIndex: false,
      email,
    });
  } else {
    res.status(200).json({ jwt_token: token });
  };
}));

router.post('/forgot_password', asyncWrapper(async (req, res) => {
  const { email } = req.body;

  await changePassword(email);

  res.status(200).json({ message: 'New password sent to your email address' });
}));

module.exports = {
  authRouter: router,
};
