/* eslint-disable camelcase */
/* eslint object-curly-spacing: ["error", "always"] */

const express = require('express');
const router = new express.Router();

const {
  infoUser,
  deleteUser,
  changePassword,
} = require('../services/usersService');

const { asyncWrapper } = require('../utils/apiUtils');

router.get('/', asyncWrapper(async (req, res) => {
  const { created_by, email } = req.user;

  const user = await infoUser(created_by);

  if (req.headers.cookie || req.headers.pragma) {
    res.render('index', {
      title: 'Profile info',
      isProfile: true,
      LOGIN: true,
      isIndex: false,
      user,
      email,
    });
  } else {
    res.status(200).json({ user });
  }
}));

router.delete('/', asyncWrapper(async (req, res) => {
  const { created_by } = req.user;

  await deleteUser(created_by);

  res.status(200).json({ message: 'Profile deleted successfully' });
}));

router.patch('/password', asyncWrapper(async (req, res) => {
  const { created_by } = req.user;

  const { oldPassword, newPassword } = req.body;

  await changePassword(created_by, oldPassword, newPassword);

  res.status(200).json({ message: 'Password changed successfully' });
}));

module.exports = {
  usersRouter: router,
};
