const usersRole = {
  DRIVER: 'DRIVER',
  SHIPPER: 'SHIPPER',
};


const SPRINTER = {
  type: 'SPRINTER',
  dimensions: {
    width: 500,
    length: 1000,
    height: 500,
  },
  load_bearing_capacity: 1000,
};
const SMALLSTRAIGHT = {
  type: 'SMALL STRAIGHT',
  dimensions: {
    width: 750,
    length: 1500,
    height: 750,
  },
  load_bearing_capacity: 1500,
};
const LARGESTRAIGHT = {
  type: 'LARGE STRAIGHT',
  dimensions: {
    width: 800,
    length: 2000,
    height: 800,
  },
  load_bearing_capacity: 2500,
};

module.exports = {
  usersRole,
  SPRINTER,
  SMALLSTRAIGHT,
  LARGESTRAIGHT,
};
