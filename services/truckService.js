/* eslint-disable camelcase */
/* eslint object-curly-spacing: ["error", "always"] */

const { Truck } = require('../models/truckModel');

const {
  SPRINTER,
  SMALLSTRAIGHT,
  LARGESTRAIGHT,
} = require('../config');

const getUsersTrucks = async (created_by) => {
  const trucks = await Truck.find({ created_by }, '-__v');

  if (!trucks) {
    throw new Error('No trucks with such user found');
  }

  return trucks;
};

const addTruckForUser = async (created_by, type) => {
  const truck = new Truck({ ...type, created_by });

  await truck.save();
};

const getUsersTruckById = async (truckId, created_by) => {
  const truck = await Truck.findOne({ _id: truckId, created_by }, '-__v');

  if (!truck) {
    throw new Error('No truck with such id found!');
  };

  return truck;
};

const updateUsersTruckById = async (truckId, created_by, data) => {
  const truck = await Truck.findOne({ _id: truckId, created_by });

  if (!truck) {
    throw new Error('No truck with such id found!');
  };

  if (truck.status === 'OL') {
    throw new Error('You cannot perform these actions while loaded');
  };

  if (truck.assigned_to === created_by) {
    throw new Error('You can only update not assigned to you trucks');
  } else {
    if (data.type) {
      switch (data.type) {
        case 'SPRINTER':
          await Truck.findOneAndUpdate({
            _id: truckId, created_by,
          }, { $set: SPRINTER });
          break;
        case 'SMALL STRAIGHT':
          await Truck.findOneAndUpdate({
            _id: truckId, created_by,
          }, { $set: SMALLSTRAIGHT });
          break;
        case 'LARGE STRAIGHT':
          await Truck.findOneAndUpdate({
            _id: truckId, created_by,
          }, { $set: LARGESTRAIGHT });
          break;
        default:
          break;
      };
    } else {
      await Truck.findOneAndUpdate({
        _id: truckId, created_by,
      }, { $set: data });
    };
  }
};

const deleteUsersTruckById = async (truckId, created_by) => {
  const truck = await Truck.findOne({ _id: truckId, created_by });

  if (!truck) {
    throw new Error('No truck with such id found!');
  };

  if (truck.status === 'OL') {
    throw new Error('You cannot perform these actions while loaded');
  };

  if (truck.assigned_to === created_by) {
    throw new Error('You can only delete not assigned to you trucks');
  } else {
    await Truck.findOneAndRemove({ _id: truckId, created_by });
  }
};

const assignTruckToUserById = async (truckId, created_by) => {
  const truck = await Truck.findOne({ _id: truckId, created_by });

  if (!truck) {
    throw new Error('No truck with such id found!');
  };

  if (truck.status === 'OL') {
    throw new Error('You cannot perform these actions while loaded');
  };

  await truck.updateOne({
    $set: {
      assigned_to: created_by,
    },
  });
};

module.exports = {
  getUsersTrucks,
  addTruckForUser,
  getUsersTruckById,
  updateUsersTruckById,
  deleteUsersTruckById,
  assignTruckToUserById,
};
