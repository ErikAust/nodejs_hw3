/* eslint-disable camelcase */
/* eslint object-curly-spacing: ["error", "always"] */

const { Load } = require('../models/loadModel');
const { Truck } = require('../models/truckModel');

const { usersRole } = require('../config');

const getLoads = async (created_by, offset = 0, limit = 10, status, role) => {
  if (limit > 50) {
    throw new Error('The limit should not be more than 50');
  };

  if (role === usersRole.SHIPPER) {
    const loads = await Load.find({
      created_by,
    }, '-__v').limit(+limit).skip(+offset);

    return loads;
  } else {
    if (status) {
      const loads = await Load.find({
        created_by, status: status,
      }, '-__v').limit(+limit).skip(+offset);

      return loads;
    } else {
      const loads = await Load.find({
        assigned_to: created_by,
      }, '-__v').limit(+limit).skip(+offset);

      return loads;
    }
  }
};

const addLoadForUser = async (created_by, payLoad) => {
  const load = new Load({ ...payLoad, created_by });

  await load.save();
};

const getUsersActiveLoad = async (created_by) => {
  const load = await Load.findOne({
    assigned_to: created_by, status: 'ASSIGNED',
  });

  if (!load) {
    throw new Error('No active load found');
  };

  return load;
};

const changeLoadState = async (created_by) => {
  let load = await Load.findOne({
    assigned_to: created_by, status: 'ASSIGNED',
  });

  if (!load) {
    throw new Error('No active load found');
  };

  switch (load.state) {
    case 'Arrived to Pick Up':
      await load.updateOne({
        $set: {
          state: 'En route to delivery',
        },
        $push: {
          logs: {
            message: `Load state changed to 'En route to delivery'`,
            time: new Date(Date.now()),
          },
        },
      });
      break;
    case 'En route to Pick Up':
      await load.updateOne({
        $set: {
          state: 'Arrived to Pick Up',
        },
        $push: {
          logs: {
            message: `Load state is changed to 'Arrived to Pick Up'`,
            time: new Date(Date.now()),
          },
        },
      });
      break;
    case 'En route to delivery':
      await load.updateOne({
        $set: {
          state: 'Arrived to delivery',
          status: 'SHIPPED',
        },
        $push: {
          logs: {
            message: `Load state changed to 'Arrived to delivery'`,
            time: new Date(Date.now()),
          },
        },
      });

      await Truck.findOneAndUpdate({ created_by: load.assigned_to }, {
        $set: {
          status: 'IS',
        },
      });
      break;
    default:
      break;
  };

  load = await Load.findOne({ _id: load._id });

  return load.state;
};

const getUsersloadById = async (loadId, created_by) => {
  const load = await Load.findOne({ _id: loadId, created_by }, '-__v');

  if (!load) {
    throw new Error('No load with such user found');
  };

  return load;
};

const updateUsersLoadById = async (loadId, created_by, data) => {
  const load = await Load.findOne({ _id: loadId, created_by });

  if (!load) {
    throw new Error('No load with such id found!');
  };

  if (load.status !== 'NEW') {
    throw new Error(`You can only update info about loads with status 'NEW'`);
  } else {
    await Load.findOneAndUpdate({ _id: loadId, created_by }, { $set: data });
  }
};

const deleteUsersLoadById = async (loadId, created_by) => {
  const load = await Load.findOne({ _id: loadId, created_by });

  if (!load) {
    throw new Error('No truck with such id found!');
  };

  if (load.status !== 'NEW') {
    throw new Error(`You can only delete loads with status 'NEW'`);
  } else {
    await Load.findOneAndRemove({ _id: loadId, created_by });
  }
};

const postUsersLoadById = async (loadId) => {
  const load = await Load.findOne({ _id: loadId });

  if (!load) {
    throw new Error('No load with such id found!');
  };

  await load.updateOne({
    $set: {
      status: 'POSTED',
    },
    $push: {
      logs: {
        message: `Load status changed to 'POSTED'`,
        time: new Date(Date.now()),
      },
    },
  });

  const truck = await Truck.findOne({
    status: 'IS',
    assigned_to: { $ne: null },
  });

  let driver_found = false;
  let idDriver;

  if (truck && load.payload < truck.load_bearing_capacity &&
    load.dimensions.width < truck.dimensions.width &&
    load.dimensions.length < truck.dimensions.length &&
    load.dimensions.height < truck.dimensions.height) {
    await truck.updateOne({
      $set: {
        status: 'OL',
      },
    });

    idDriver = truck.assigned_to;

    await load.updateOne({
      $set: {
        status: 'ASSIGNED',
        state: 'En route to Pick Up',
        assigned_to: idDriver,
      },
      $push: {
        logs: {
          message: `Load assigned to driver with id ${idDriver}`,
          time: new Date(Date.now()),
        },
      },
    });

    driver_found = true;

    return driver_found;
  } else {
    await load.updateOne({
      $set: {
        status: 'NEW',
      },
      $push: {
        logs: {
          message: 'Driver not found',
          time: new Date(Date.now()),
        },
      },
    });
    return driver_found;
  };
};

const getUsersloadShippingById = async (loadId, created_by) => {
  const load = await Load.findOne({ _id: loadId, created_by }, '-__v');

  if (load.status === 'NEW' || load.status === 'POSTED') {
    throw new Error('No exist loads shipping');
  } else {
    return load;
  }
};

module.exports = {
  addLoadForUser,
  getLoads,
  getUsersActiveLoad,
  changeLoadState,
  getUsersloadById,
  updateUsersLoadById,
  deleteUsersLoadById,
  postUsersLoadById,
  getUsersloadShippingById,
};
