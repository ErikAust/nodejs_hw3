/* eslint object-curly-spacing: ["error", "always"] */

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const { User } = require('../models/userModel');
const { SECRET } = require('../middlewares/authMiddleware');

const register = async ({ email, password, role }) => {
  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
  });

  await user.save();
};

const login = async ({ email,
  password }) => {
  const user = await User.findOne({ email });

  if (!user) {
    throw new Error('Invalid email or password');
  }

  if (!(await bcrypt.compare(password, user.password))) {
    throw new Error('Invalid email or password');
  }

  const token = jwt.sign({
    _id: user._id,
    email: user.email,
    role: user.role,
  }, SECRET);

  return token;
};

const changePassword = async (email) => {
  const user = await User.findOne({ email });

  if (!user) {
    throw new Error('Wrong email!');
  };
};

module.exports = {
  register,
  login,
  changePassword,
};
