/* eslint-disable camelcase */
/* eslint object-curly-spacing: ["error", "always"] */

const bcrypt = require('bcrypt');

const { User } = require('../models/userModel');
const { Truck } = require('../models/truckModel');

const infoUser = async (created_by) => {
  const user = await User.findOne({ _id: created_by }, '-__v -password');

  if (!user) {
    throw new Error('No user found');
  }
  return user;
};

const deleteUser = async (created_by) => {
  const truck = await Truck.findOne({ created_by });

  if (truck && truck.status === 'OL') {
    throw new Error('You cannot perform these actions while loaded');
  };

  const user = await User.findOneAndDelete({ _id: created_by });

  if (!user) {
    throw new Error('No user found');
  }
  return user;
};

const changePassword = async (created_by, oldPassword, newPassword) => {
  const truck = await Truck.findOne({ created_by });

  if (truck && truck.status === 'OL') {
    throw new Error('You cannot perform these actions while loaded');
  };

  const user = await User.findOne({ _id: created_by });

  if (!user) {
    throw new Error('No user found');
  };

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    throw new Error('Wrong password!');
  }

  await User.updateOne({ _id: created_by }, {
    $set: {
      password: await bcrypt.hash(newPassword, 10),
    },
  });
};

module.exports = {
  infoUser,
  deleteUser,
  changePassword,
};
