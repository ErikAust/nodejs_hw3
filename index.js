/* eslint object-curly-spacing: ["error", "always"] */

const express = require('express');
const app = express();
const morgan = require('morgan');
const path = require('path');
const mongoose = require('mongoose');
const exphbs = require('express-handlebars');

const hbs = exphbs.create({
  defaultLayout: 'main',
  extname: 'hbs',
});
const cookieParser = require('cookie-parser');

require('dotenv').config();
const PORT = process.env.PORT || 8080;

const { loadRouter } = require('./controllers/loadController');
const { truckRouter } = require('./controllers/truckController');
const { usersRouter } = require('./controllers/usersController');
const { authRouter } = require('./controllers/authController');
const { authMiddleware } = require('./middlewares/authMiddleware');
const { regMiddleware } = require('./middlewares/registrationMiddleware');
const { driverRole } = require('./middlewares/driverMiddleware');

const { mainRouter } = require('./controllers/mainRouter');

app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');
app.set('views', 'views');

app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json());
app.use(morgan('tiny'));
app.use(cookieParser());
app.use('/', mainRouter);
app.use('/api/auth', [regMiddleware], authRouter);
app.use(authMiddleware);
app.use('/api', mainRouter);
app.use('/api/users/me', usersRouter);
app.use('/api/loads', loadRouter);
app.use(driverRole);
app.use('/api/trucks', truckRouter);

app.use((req, res, next) => {
  res.status(404).json({ message: 'Not found' });
});
app.use((err, req, res, next) => {
  res.status(500).json({ message: err.message });
});

const start = async () => {
  try {
    await mongoose.connect('mongodb+srv://user1:qwery@cluster0.eulgm.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    });
    app.listen(PORT);
  } catch (err) {
    console.error(err.message);
  }
};

start();
