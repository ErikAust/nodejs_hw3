/* eslint object-curly-spacing: ["error", "always"] */

const joi = require('joi');

const regMiddleware = async (req, res, next) => {
  const template = joi.object({
    email: joi.string().email(),

    password: joi.string().min(5).max(15),

    role: joi.string(),
  });

  try {
    await template.validateAsync(req.body);
    next();
  } catch (err) {
    next(new Error(err.message));
  }
};

module.exports = { regMiddleware };
