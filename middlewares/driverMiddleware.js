/* eslint object-curly-spacing: ["error", "always"] */

const driverRole = (req, res, next) => {
  try {
    const { role } = req.user;

    if (role !== 'DRIVER') {
      throw new Error('Your role is not authorized to perform these actions');
    }
    next();
  } catch (err) {
    res.status(401).json({ message: err.message });
  }
};

module.exports = { driverRole };
